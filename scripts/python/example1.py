from jnpr.junos import Device
from lxml import etree
import jxmlease

class JunosDevice(Device):
    def __init__(self,
                 hostname,
                 username,
                 password,
                 port=830):
        super().__init__(host=hostname,
                         user=username,
                         passwd=password,
                         port=port,
                         normalize=True,
                         )
        self.open()

def main():
    dev_name = "192.168.100.100"
    password = "Cisco123456"
    print(F"ssh root: {dev_name}")
    dev = JunosDevice(dev_name, "noc", password)
    data = dev.rpc.get_config(filter_xml='protocols/bgp', options={'format': 'xml'})
    rpc_xml = etree.tostring(data, pretty_print=True, encoding='unicode')
    result = jxmlease.parse(rpc_xml)
    print(result)
    dev.close()

if "__main__":
    main()
