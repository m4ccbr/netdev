tell application "Microsoft Outlook"
    set theMessages to messages of inbox
    repeat with theMessage in theMessages
        set theSender to sender of theMessage
        set theSenderName to name of theSender as string
        set eMail to address of theSender
        set theSubject to subject of theMessage
        if is read of theMessage is false then
            set theSubject to ""
            set theContent to ""
            set newMessage to make new outgoing message with properties {subject:theSubject, content:theContent}
            make new recipient at newMessage with properties {email address:{address:eMail}}
            send newMessage
            set is read of theMessage to true
        end if
    end repeat
end tell
